/* Pakiet zawiera funkcje zamieniając informacje w postaci łańcuchów znaków
 * na odpowiednie numeryczne wartości.
 */

CREATE OR REPLACE PACKAGE id_conversion AS
  FUNCTION getDozwoloneID(nazwa IN VARCHAR2) RETURN NUMBER;
  FUNCTION getRolaID(nazwa IN VARCHAR2) RETURN NUMBER;
  FUNCTION getKrajID(nazwa IN VARCHAR2) RETURN NUMBER;
  FUNCTION getFilmID(nazwa IN VARCHAR2)  RETURN NUMBER;
  FUNCTION getSerialID(nazwa IN VARCHAR2) RETURN NUMBER;
  FUNCTION getProgramID(nazwa IN VARCHAR2) RETURN NUMBER;
  FUNCTION getTransmisjaID(tytul IN VARCHAR2) RETURN NUMBER;
  FUNCTION getStudioID(nazwa IN VARCHAR2) RETURN NUMBER;
  FUNCTION getOsobaID(first_name IN VARCHAR2, surname IN VARCHAR2) RETURN NUMBER;
  FUNCTION getWlasciwoscID(nazwa IN VARCHAR2) RETURN NUMBER;
  FUNCTION getRodzajID(nazwa IN VARCHAR2) RETURN NUMBER;
  FUNCTION getKontoID(nick IN VARCHAR2) RETURN NUMBER;
END id_conversion;
/

CREATE OR REPLACE PACKAGE BODY id_conversion AS

  FUNCTION getDozwoloneID(nazwa IN VARCHAR2)
  RETURN NUMBER
  IS
    ID NUMBER;
  BEGIN
    SELECT ID_OdLat INTO ID FROM T_Dozwolone WHERE nazwa = Lata;
    RETURN ID;
  END;
  
  FUNCTION getRolaID(nazwa IN VARCHAR2)
  RETURN NUMBER
  IS
    ID NUMBER;
  BEGIN
    SELECT ID_Rola INTO ID FROM T_Rola WHERE Nazwa_roli = nazwa;
    RETURN ID;
  END;

  FUNCTION getKrajID(nazwa IN VARCHAR2)
  RETURN NUMBER
  IS
    ID NUMBER;
  BEGIN
    SELECT ID_Kraj INTO ID FROM T_Kraj WHERE Kraj = nazwa;
    RETURN ID;
  END;

  FUNCTION getFilmID(nazwa IN VARCHAR2) -- uzywamy oryginalnych tytulow
  RETURN NUMBER
  IS
    ID NUMBER;
  BEGIN
    SELECT ID_Film INTO ID FROM T_Film WHERE Tytul_Orig = nazwa;
    RETURN ID;
  END;

  FUNCTION getSerialID(nazwa IN VARCHAR2)
  RETURN NUMBER
  IS
    ID NUMBER;
  BEGIN
    SELECT ID_Serial INTO ID FROM T_Serial WHERE Tytul_Orig = nazwa;
    RETURN ID;
  END;

  FUNCTION getProgramID(nazwa IN VARCHAR2)
  RETURN NUMBER
  IS
    ID NUMBER;
  BEGIN
    SELECT ID_Program INTO ID FROM T_Program WHERE Tytul = nazwa;
    RETURN ID;
  END;

  FUNCTION getTransmisjaID(tytul IN VARCHAR2)
  RETURN NUMBER
  IS
    ID NUMBER;
  BEGIN
    SELECT ID_Transmisja INTO ID FROM T_Transmisja WHERE Nazwa = tytul;
    RETURN ID;
  END;

  FUNCTION getStudioID(nazwa IN VARCHAR2)
  RETURN NUMBER
  IS
    ID NUMBER;
  BEGIN
    SELECT ID_Studio INTO ID FROM T_Studio WHERE nazwa = Studio;
    RETURN ID;
  END;

  FUNCTION getOsobaID(first_name IN VARCHAR2, surname IN VARCHAR2)
  RETURN NUMBER
  IS
    ID NUMBER;
  BEGIN
    SELECT ID_Osoba INTO ID FROM T_Osoba WHERE Imie = first_name AND Nazwisko = surname;
    RETURN ID;
  END;

  FUNCTION getWlasciwoscID(nazwa IN VARCHAR2)
  RETURN NUMBER
  IS
    ID NUMBER;
  BEGIN
    SELECT ID_Wlasciwosc INTO ID FROM T_Wlasciwosc WHERE nazwa = Nazwa_wlasciwosci;
    RETURN ID;
  END;

  FUNCTION getRodzajID(nazwa IN VARCHAR2)
  RETURN NUMBER
  IS
    ID NUMBER;
  BEGIN
    SELECT ID_Rodzaj INTO ID FROM T_Rodzaj WHERE nazwa = Rodzaj;
    RETURN ID;
  END;

  FUNCTION getKontoID(nick IN VARCHAR2)
  RETURN NUMBER
  IS
    ID NUMBER;
  BEGIN
    SELECT ID_Konto INTO ID FROM T_Konto WHERE Nazwa_konta = nick;
    RETURN ID;
  END;
END id_conversion;
/
