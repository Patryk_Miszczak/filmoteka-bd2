/* Kursory służące do przeszukiwania bazy pod kątem określonych kryteriów. */

CURSOR gatunek_material(gatunek VARCHAR2) IS
	select ID_Material from t_map_rodzaj_material where id_rodzaj = id_conversion.getRodzajID(gatunek);

CURSOR osoba_film(tytul VARCHAR2) IS
	select ID_Osoba from t_map_osoba_material where id_material = id_conversion.getFilmID(tytul);

CURSOR osoba_serial(tytul VARCHAR2) IS
	select ID_Osoba from t_map_osoba_material where id_material = id_conversion.getSerialID(tytul);

CURSOR osoba_program(tytul VARCHAR2) IS
	select ID_Osoba from t_map_osoba_material where id_material = id_conversion.getProgramID(tytul);
