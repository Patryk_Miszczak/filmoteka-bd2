/* Triggery dbające o zapewnianie nowych id przy wstawianiu rekordów. */

CREATE OR REPLACE TRIGGER gen_ID_Osoba
  BEFORE INSERT ON T_Osoba
  FOR EACH ROW
BEGIN
  :new.ID_Osoba := seq_ID_Osoba.nextval;
END;
/

CREATE OR REPLACE TRIGGER gen_ID_Studio
  BEFORE INSERT ON T_Studio
  FOR EACH ROW
BEGIN
  :new.ID_Studio := seq_ID_Studio.nextval;
END;
/

CREATE OR REPLACE TRIGGER gen_ID_Kraj
  BEFORE INSERT ON T_Kraj
  FOR EACH ROW
BEGIN
  :new.ID_Kraj := seq_ID_Kraj.nextval;
END;
/

CREATE OR REPLACE TRIGGER gen_ID_Film
  BEFORE INSERT ON T_Film
  FOR EACH ROW
BEGIN
  :new.ID_Film := seq_ID_Film.nextval;
END;
/

CREATE OR REPLACE TRIGGER gen_ID_Serial_Odcinek
  BEFORE INSERT ON T_Serial_Odcinek
  FOR EACH ROW
BEGIN
  :new.ID_Serial_Odcinek := seq_ID_Serial_Odcinek.nextval;
END;
/

CREATE OR REPLACE TRIGGER gen_ID_Serial
  BEFORE INSERT ON T_Serial
  FOR EACH ROW
BEGIN
  :new.ID_Serial := seq_ID_Serial.nextval;
END;
/

CREATE OR REPLACE TRIGGER gen_ID_Program
  BEFORE INSERT ON T_Program
  FOR EACH ROW
BEGIN
  :new.ID_Program := seq_ID_Program.nextval;
END;
/

CREATE OR REPLACE TRIGGER gen_ID_Transmisja
  BEFORE INSERT ON T_Transmisja
  FOR EACH ROW
BEGIN
  :new.ID_Transmisja := seq_ID_Transmisja.nextval;
END;
/

CREATE OR REPLACE TRIGGER gen_ID_Konto
  BEFORE INSERT ON T_Konto
  FOR EACH ROW
BEGIN
  IF :new.ID_Konto_Matka IS NOT NULL THEN
    :new.ID_Konto := seq_ID_PodKonto.nextval;
  ELSE
    :new.ID_Konto := seq_ID_Konto.nextval;
  END IF;
END;
/

CREATE OR REPLACE TRIGGER gen_ID_Rola
  BEFORE INSERT ON T_Rola
  FOR EACH ROW
BEGIN
  :new.ID_Rola := seq_ID_Rola.nextval;
END;
/

CREATE OR REPLACE TRIGGER gen_ID_Rodzaj
  BEFORE INSERT ON T_Rodzaj
  FOR EACH ROW
BEGIN
  :new.ID_Rodzaj := seq_ID_Rodzaj.nextval;
END;
/
