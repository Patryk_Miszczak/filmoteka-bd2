/* Konta. */

--INSERT INTO T_Konto (Nazwa_konta, Adres_mail) VALUES ();

INSERT INTO T_Konto (Nazwa_konta, Adres_mail) VALUES ('mirek121', 'mirek145@poczta.pl');
INSERT INTO T_Konto (Nazwa_konta, Adres_mail) VALUES ('lubieplacki', 'zdzichu456@poczta.pl');
INSERT INTO T_Konto (Nazwa_konta, Adres_mail) VALUES ('jakbedziewakapie', 'anotheraccount@poczta.pl');
INSERT INTO T_Konto (Nazwa_konta, Adres_mail) VALUES ('jetfuelcantmeltsteelbeams', 'spiseg@poczta.pl');

/* Uzytkownicy potwierdzonych kont. */

--INSERT INTO T_Uzytkownik (ID_Konto, Imie, Nazwisko, Nr_telefonu) VALUES (); 
INSERT INTO T_Uzytkownik (ID_Konto, Imie, Nazwisko, Nr_telefonu) VALUES (id_conversion.getKontoID('mirek121'), 'Mirosław', 'Baka', '1235678'); 

/* Filmy. */

--INSERT INTO T_Film (Tytul, Tytul_Orig, ID_OdLat, Rok_prod, Data_premiery, Czas)  VALUES (); 

INSERT INTO T_Film (Tytul, Tytul_Orig, ID_OdLat, Rok_prod, Data_premiery, Czas) VALUES ('Władca Pierścieni: Drużyna Pierścienia','The Lord of the Rings: The Fellowship of the Ring', 3, 2001, '2001-12-19', 178); 
INSERT INTO T_Film (Tytul, Tytul_Orig, ID_OdLat, Rok_prod, Data_premiery, Czas) VALUES ('Przeminęło z wiatrem', 'Gone With The Wind', 3, 1939, '1940-01-17', 238); 
INSERT INTO T_Film (Tytul, Tytul_Orig, ID_OdLat, Rok_prod, Data_premiery, Czas) VALUES ('Psy', 'Psy', 5, 1992, '1992-12-31', 101); 
INSERT INTO T_Film (Tytul, Tytul_Orig, ID_OdLat, Rok_prod, Data_premiery, Czas) VALUES ('Królestwo', 'Les saisons', 1, 2015, '2015-10-23', 97); 

/* Seriale. */

--INSERT INTO T_Serial (Tytul, Tytul_Orig, ID_OdLat, Ilosc_sezonow) VALUES (); 

INSERT INTO T_Serial (Tytul, Tytul_Orig, ID_OdLat, Ilosc_sezonow) VALUES ('Prawo ulicy', 'The Wire', 5, 5); 
INSERT INTO T_Serial (Tytul, Tytul_Orig, ID_OdLat, Ilosc_sezonow) VALUES ('Futurama: Przygody Fry''a w kosmosie', 'Futurama', 3, 7); 

/* Programy. */

--INSERT INTO T_Program (Tytul, Tytul_Orig, ID_OdLat) VALUES ();

INSERT INTO T_Program (Tytul, Tytul_Orig, ID_OdLat) VALUES ('Skandaliści', 'Skandaliści', 4);
INSERT INTO T_Program (Tytul, Tytul_Orig, ID_OdLat) VALUES ('Jeden z dziesięciu', 'Jeden z dziesięciu', 1);

/* Transmisje. */

--INSERT INTO T_Transmisja (Nazwa, Czas_transmisji, ID_OdLat) VALUES ();

INSERT INTO T_Transmisja (Nazwa, Czas_transmisji, ID_OdLat) VALUES ('Snooker: Turniej Masters w Londynie', '2017-06-02 03:00:00', 1);

/* Aktorzy, muzycy, reżyserowie etc. */

--INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ();

INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Peter', 'Jackson', '1961-10-31', 'M', id_conversion.getKrajID('Nowa Zelandia'));
INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Elijah', 'Wood', '1981-01-28', 'M', id_conversion.getKrajID('Stany Zjednoczone Ameryki'));
INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Ian', 'McKellen', '1939-05-25', 'M', id_conversion.getKrajID('Wielka Brytania'));
INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Orlando', 'Bloom', '1977-01-13', 'M', id_conversion.getKrajID('Wielka Brytania'));
INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Christopher', 'Lee', '1922-05-27', 'M', id_conversion.getKrajID('Wielka Brytania'));
INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Liv', 'Tyler', '1977-07-01', 'F', id_conversion.getKrajID('Stany Zjednoczone Ameryki'));
INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Howard', 'Shore', '1946-10-18', 'M', id_conversion.getKrajID('Kanada'));

INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Victor', 'Fleming', '1889-02-23', 'M', id_conversion.getKrajID('Stany Zjednoczone Ameryki'));
INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Thomas', 'Mitchell', '1892-07-11', 'M', id_conversion.getKrajID('Stany Zjednoczone Ameryki'));
INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Barbara', 'O''Neil', '1910-07-17', 'F', id_conversion.getKrajID('Stany Zjednoczone Ameryki'));
INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Vivien', 'Leigh', '1913-11-05', 'F', id_conversion.getKrajID('Indie'));

INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Władysław', 'Pasikowski', '1959-06-14', 'M', id_conversion.getKrajID('Polska'));
INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Bogusław', 'Linda', '1952-06-27', 'M', id_conversion.getKrajID('Polska'));
INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Marek', 'Kondrat', '1950-10-18', 'M', id_conversion.getKrajID('Polska'));
INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Cezary', 'Pazura', '1962-06-13', 'M', id_conversion.getKrajID('Polska'));
INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Janusz', 'Gajos', '1939-09-23', 'M', id_conversion.getKrajID('Polska'));

INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Dominic', 'West', '1969-10-15', 'M', id_conversion.getKrajID('Wielka Brytania'));
INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Deirdre', 'Lovejoy', '1962-06-30', 'F', id_conversion.getKrajID('Stany Zjednoczone Ameryki'));
INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Michael', 'Williams', '1966-11-22', 'M', id_conversion.getKrajID('Stany Zjednoczone Ameryki'));

INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) VALUES ('Krystyna', 'Czubówna', '1954-08-02', 'F', id_conversion.getKrajID('Polska'));

/* Studia filmowe. */

--INSERT INTO T_Studio (Studio, ID_Kraj) VALUES ();

INSERT INTO T_Studio (Studio, ID_Kraj) VALUES ('New Line Cinema', id_conversion.getKrajID('Stany Zjednoczone Ameryki'));
INSERT INTO T_Studio (Studio, ID_Kraj) VALUES ('Metro-Goldwyn-Mayer Pictures', id_conversion.getKrajID('Stany Zjednoczone Ameryki'));
INSERT INTO T_Studio (Studio, ID_Kraj) VALUES ('Zebra', id_conversion.getKrajID('Polska'));

/* Sezony seriali. */

--INSERT INTO T_Serial_Sezon (ID_Serial, Nr_sezonu, Start_emisji, Koniec_emisji) VALUES ();

INSERT INTO T_Serial_Sezon (ID_Serial, Nr_sezonu, Start_emisji, Koniec_emisji) VALUES (getSerialID('The Wire'), 1, '2002-06-02', '2002-09-08');
INSERT INTO T_Serial_Sezon (ID_Serial, Nr_sezonu, Start_emisji, Koniec_emisji) VALUES (getSerialID('Futurama'), 1, '1999-03-28', '1999-05-18');

/* Odcinki seriali. */

--INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES ();

INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('The Wire'), 1, 1, 'The Target', 62, '2002-06-02');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('The Wire'), 1, 2, 'The Detail', 62, '2002-06-09');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('The Wire'), 1, 3, 'The Buys', 62, '2002-06-16');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('The Wire'), 1, 4, 'Old Cases', 62, '2002-06-23');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('The Wire'), 1, 5, 'The Pager', 62, '2002-06-30');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('The Wire'), 1, 6, 'The Wire', 62, '2002-07-07');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('The Wire'), 1, 7, 'One Arrest', 62, '2002-07-21');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('The Wire'), 1, 8, 'Lessons', 62, '2002-07-28');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('The Wire'), 1, 9, 'Game Day', 62, '2002-08-04');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('The Wire'), 1, 10, 'The Cost', 62, '2002-08-11');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('The Wire'), 1, 11, 'The Hunt', 62, '2002-08-18');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('The Wire'), 1, 12, 'Cleaning Up', 62, '2002-09-01');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('The Wire'), 1, 13, 'Sentencing', 62, '2002-09-08');

INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('Futurama'), 1, 1, 'Space Pilot 3000', 30, '1999-03-28');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('Futurama'), 1, 2, 'The Series Has Landed', 30, '1999-04-04');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('Futurama'), 1, 3, 'I, Roommate', 30, '1999-04-06');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('Futurama'), 1, 4, 'Love''s Labours Lost in Space', 30, '1999-04-13');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('Futurama'), 1, 5, 'Fear of A Bot Planet', 30, '1999-04-20');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('Futurama'), 1, 6, 'A Fishful of Dollars', 30, '1999-04-27');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('Futurama'), 1, 7, 'My Three Suns', 30, '1999-05-04');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('Futurama'), 1, 8, 'A Big Piece of Garbage', 30, '1999-05-11');
INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) VALUES (
	getSerialID('Futurama'), 1, 9, 'Hell Is Other Robots', 30, '1999-05-18');
