/* Inserty do tabel pomocniczych. */

/* Ludzie do materiałów. */

--INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material) VALUES ();
--INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material, Odgrywana_rola) VALUES ();

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material) VALUES ( 
  id_conversion.getOsobaID('Peter', 'Jackson'), id_conversion.getRolaID('Reżyser'), id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'));

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material, Odgrywana_rola) VALUES ( 
  id_conversion.getOsobaID('Elijah', 'Wood'), id_conversion.getRolaID('Aktor'), id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'), 'Frodo Baggins');

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material, Odgrywana_rola) VALUES ( 
  id_conversion.getOsobaID('Orlando', 'Bloom'), id_conversion.getRolaID('Aktor'), id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'), 'Legolas');

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material, Odgrywana_rola) VALUES ( 
  id_conversion.getOsobaID('Ian', 'McKellen'), id_conversion.getRolaID('Aktor'), id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'), 'Gandalf');

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material, Odgrywana_rola) VALUES ( 
  id_conversion.getOsobaID('Christopher', 'Lee'), id_conversion.getRolaID('Aktor'), id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'), 'Saruman');

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material, Odgrywana_rola) VALUES ( 
  id_conversion.getOsobaID('Liv', 'Tyler'), id_conversion.getRolaID('Aktor'), id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'), 'Arwen');

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material) VALUES ( 
  id_conversion.getOsobaID('Howard', 'Shore'), id_conversion.getRolaID('Muzyka'), id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'));

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material) VALUES ( 
  id_conversion.getOsobaID('Victor', 'Fleming'), id_conversion.getRolaID('Reżyser'), id_conversion.getFilmID('Gone With The Wind'));

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material, Odgrywana_rola) VALUES ( 
  id_conversion.getOsobaID('Thomas', 'Mitchell'), id_conversion.getRolaID('Aktor'), id_conversion.getFilmID('Gone With The Wind'), 'Gerald O''Hara');

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material, Odgrywana_rola) VALUES ( 
  id_conversion.getOsobaID('Barbara', 'O''Neil'), id_conversion.getRolaID('Aktor'), id_conversion.getFilmID('Gone With The Wind'), 'Ellen O''Hara');

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material, Odgrywana_rola) VALUES ( 
  id_conversion.getOsobaID('Vivien', 'Leigh'), id_conversion.getRolaID('Aktor'), id_conversion.getFilmID('Gone With The Wind'), 'Scarlett O''Hara');

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material) VALUES ( 
  id_conversion.getOsobaID('Władysław', 'Pasikowski'), id_conversion.getRolaID('Scenariusz'), id_conversion.getFilmID('Psy'));

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material) VALUES ( 
  id_conversion.getOsobaID('Władysław', 'Pasikowski'), id_conversion.getRolaID('Reżyser'), id_conversion.getFilmID('Psy'));

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material, Odgrywana_rola) VALUES ( 
  id_conversion.getOsobaID('Cezary', 'Pazura'), id_conversion.getRolaID('Aktor'), id_conversion.getFilmID('Psy'), 'Waldemar Morawiec');

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material, Odgrywana_rola) VALUES ( 
  id_conversion.getOsobaID('Bogusław', 'Linda'), id_conversion.getRolaID('Aktor'), id_conversion.getFilmID('Psy'), 'Franciszek Maurer');

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material, Odgrywana_rola) VALUES ( 
  id_conversion.getOsobaID('Marek', 'Kondrat'), id_conversion.getRolaID('Aktor'), id_conversion.getFilmID('Psy'), 'Olgierd Żwirski');

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material, Odgrywana_rola) VALUES ( 
  id_conversion.getOsobaID('Janusz', 'Gajos'), id_conversion.getRolaID('Aktor'), id_conversion.getFilmID('Psy'), 'major Gross');

INSERT INTO T_MAP_Osoba_Material (ID_Osoba, ID_Rola, ID_Material) VALUES ( 
  id_conversion.getOsobaID('Krystyna', 'Czubówna'), id_conversion.getRolaID('Lektor'), id_conversion.getFilmID('Les saisons'));

/* Studia do materiałów. */

--INSERT INTO T_MAP_Studio_Material (ID_Studio, ID_Material) VALUES ();

INSERT INTO T_MAP_Studio_Material (ID_Studio, ID_Material) VALUES (getStudioID('New Line Cinema'), id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'));
INSERT INTO T_MAP_Studio_Material (ID_Studio, ID_Material) VALUES (getStudioID('Metro-Goldwyn-Mayer Pictures'), id_conversion.getFilmID('Gone With The Wind'));
INSERT INTO T_MAP_Studio_Material (ID_Studio, ID_Material) VALUES (getStudioID('Zebra'), id_conversion.getFilmID('Psy'));

/* Właściwości do materiałów. */

--INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES ();

INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'), id_conversion.getWlasciwoscID('polski'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'), id_conversion.getWlasciwoscID('720p'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'), id_conversion.getWlasciwoscID('5.1'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'), id_conversion.getWlasciwoscID('Kupno'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'), id_conversion.getWlasciwoscID('Dolby Digital'));

INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getFilmID('Gone With The Wind'), id_conversion.getWlasciwoscID('polski'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getFilmID('Gone With The Wind'), id_conversion.getWlasciwoscID('480p'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getFilmID('Gone With The Wind'), id_conversion.getWlasciwoscID('5.1'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getFilmID('Gone With The Wind'), id_conversion.getWlasciwoscID('Kupno'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getFilmID('Gone With The Wind'), id_conversion.getWlasciwoscID('Dolby Digital'));

INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getFilmID('Psy'), id_conversion.getWlasciwoscID('polski'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getFilmID('Psy'), id_conversion.getWlasciwoscID('480p'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getFilmID('Psy'), id_conversion.getWlasciwoscID('5.1'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getFilmID('Psy'), id_conversion.getWlasciwoscID('Kupno'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getFilmID('Psy'), id_conversion.getWlasciwoscID('Dolby Digital'));

INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getSerialID('The Wire'), id_conversion.getWlasciwoscID('polski'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getSerialID('The Wire'), id_conversion.getWlasciwoscID('1080p'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getSerialID('The Wire'), id_conversion.getWlasciwoscID('5.1'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getSerialID('The Wire'), id_conversion.getWlasciwoscID('Kupno'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getSerialID('The Wire'), id_conversion.getWlasciwoscID('Dolby Digital'));

INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getSerialID('Futurama'), id_conversion.getWlasciwoscID('polski'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getSerialID('Futurama'), id_conversion.getWlasciwoscID('1080p'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getSerialID('Futurama'), id_conversion.getWlasciwoscID('5.1'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getSerialID('Futurama'), id_conversion.getWlasciwoscID('Kupno'));
INSERT INTO T_MAP_Material_Wlasciwosc (ID_Material, ID_Wlasciwosc) VALUES 
	(id_conversion.getSerialID('Futurama'), id_conversion.getWlasciwoscID('Dolby Digital'));





/* Rodzaje do materiałów. */

--INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES ();

INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Przygodowy'), id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'));
INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Fantasy'), id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'));
INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Akcja'), id_conversion.getFilmID('The Lord of the Rings: The Fellowship of the Ring'));

INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Romans'), id_conversion.getFilmID('Gone With The Wind'));
INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Dramat'), id_conversion.getFilmID('Gone With The Wind'));
INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Historyczny'), id_conversion.getFilmID('Gone With The Wind'));
INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Wojenny'), id_conversion.getFilmID('Gone With The Wind'));

INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Akcja'), id_conversion.getFilmID('Psy'));
INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Sensacyjny'), id_conversion.getFilmID('Psy'));

INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Sensacyjny'), id_conversion.getSerialID('The Wire'));
INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Akcja'), id_conversion.getSerialID('The Wire'));

INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Komedia'), id_conversion.getSerialID('Futurama'));
INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Kreskówka'), id_conversion.getSerialID('Futurama'));

INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Przyrodniczy'), id_conversion.getFilmID('Les saisons'));
INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Familijny'), id_conversion.getFilmID('Les saisons'));

INSERT INTO T_MAP_Rodzaj_Material (ID_Rodzaj, ID_Material) VALUES (id_conversion.getRodzajID('Sport'), getTransmisjaID('Snooker: Turniej Masters w Londynie'));
