rodzaje = [
    'Sensacyjny', 'Akcji', 'Przygodowy', 'Western', 'Baśń', 'Bajka', 'Biograficzny', 'Komedia',
    'Historyczny', 'Dokumentalny', 'Dramat', 'Dreszczowiec', 'Animowany', 'Kreskówka',
    'Erotyczny', 'Fantastyczny', 'Fantasy', 'Religijny', 'Horror', 'Naukowy', 'Kryminalny',
    'Karate', 'Wojenny', 'Szpiegowski', 'Przyrodniczy', 'Psychologiczny', 'Przyrodniczy',
    'Obyczajowy', 'Musical', 'Sci-Fi', 'Podróżniczy', 'Kulinarny', 'Talk-show', 'Sport',
    'Thriller', 'Apokaliptyczny',
    'Historyczny', 'Dla dzieci', 'Edukacyjny', 'Noire', 'Piłka nożna', 'Siatkówka',
    'Boks', 'Sporty walki', 'Golf', 'Tenis', 'Poker', 'Teatr', 'Muzyka'
    ]

for gatunek in rodzaje:
    print("INSERT INTO T_Rodzaj (Rodzaj) VALUES ('" + gatunek + "');")

role = [
    'Aktor', 'Reżyser', 'Scenariusz', 'Muzyka', 'Zdjęcia', 'Montaż', 'Scenografia', 'Kostiumy',
    'Produkcja', 'Dźwięk', 'Lektor', 'Dubbing', 'Kaskader'
    ]

for rola in role:
    print("INSERT INTO T_Rola (Rola) VALUES ('" + rola + "');")

audio_video = [
        '360p', '480p', '480i', 'DVD', '576i', '576p', '720p', '1080p', '1080i', '4K', '8K',
        '3:2', '4:3', '16:9',
        'AAC', 'AC3', 'DTS', 'Dolby Digital', 'Dolby Digital Ex', 'Dolby Digital Plus', 'Dolby Pro Logic II',
        '2.0', '2.1', '5.0', '5.1', '6.1', '7.1'
        ]

licznik = 1

for x in audio_video:
    print("INSERT INTO T_Wlasciwosc (ID_Wlasciwosc, Nazwa_wlasciwosci) VALUES (" + str(licznik) + ", '" + x + "');")
    licznik += 1

napisy = [
        'polski', 'angielski', 'niemiecki', 'francuski', 'hiszpański', 'rosyjski',
        'japoński', 'chiński', 'szwedzki', 'węgierski', 'grecki', 'portugalski'
        'włoski', 'arabski', 'hebrajski', 'ukraiński'
        ]

licznik = 101

for x in napisy:
    print("INSERT INTO T_Wlasciwosc (ID_Wlasciwosc, Nazwa_wlasciwosci) VALUES (" + str(licznik) + ", '" + x + "');")
    licznik += 1

dodatki = [
        'Edycja Reżyserska', 'Edycja Rozszerzona', 'Wywiady z aktorami', 'Za scenami', 'Wiele ujęć kamery',
        '3D', 'Dubbing', 'Lektor'
        ]

licznik = 501

for x in dodatki:
    print("INSERT INTO T_Wlasciwosc (ID_Wlasciwosc, Nazwa_wlasciwosci) VALUES (" + str(licznik) + ", '" + x + "');")
    licznik += 1

dostep = [
        'Kupno', 'Wypozyczenie', 'PPV'
        ]

licznik = 3001

for x in dostep:
    print("INSERT INTO T_Wlasciwosc (ID_Wlasciwosc, Nazwa_wlasciwosci) VALUES (" + str(licznik) + ", '" + x + "');")
    licznik += 1
