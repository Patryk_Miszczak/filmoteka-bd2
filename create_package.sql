/* Pakiet zawiera procedury do wstawiania nowych encji. */

CREATE OR REPLACE PACKAGE constructors AS

  PROCEDURE addFilm(
    nazwa IN VARCHAR2, 
    nazwa_orig IN VARCHAR2, 
    dozwolone IN VARCHAR2, 
    rok_produkcji IN NUMBER, 
    premiera IN DATE, 
    czas_trwania IN NUMBER);

  PROCEDURE addSerial(
    nazwa IN VARCHAR2, 
    nazwa_orig IN VARCHAR2, 
    dozwolone IN VARCHAR2, 
    ile_sezonow IN NUMBER);

  PROCEDURE addSerialSezon(
    nazwa_orig IN VARCHAR2, 
    numer_sezonu IN NUMBER,
    poczatek IN DATE); 

  PROCEDURE addSerialOdcinek(
    nazwa_orig IN VARCHAR2,
    numer_sezonu IN NUMBER,
    numer_odcinka IN NUMBER,
    nazwa_odcinka IN VARCHAR2,
    czas IN NUMBER,
    emisja IN DATE);

  PROCEDURE addTransmisja(
    nazwa_transmisji IN VARCHAR2,
    czas IN TIMESTAMP,
    dozwolone IN VARCHAR2);

  PROCEDURE addProgram(
    nazwa_programu IN VARCHAR2,
    nazwa_orig IN VARCHAR2,
    dozwolone IN VARCHAR2);

  PROCEDURE addOsoba(
    first_name IN VARCHAR2,
    surname IN VARCHAR2,
    urodzona IN DATE,
    gender IN CHAR,
    kraj IN VARCHAR2);

  PROCEDURE addRola(
    nazwa IN VARCHAR2);

  PROCEDURE addRodzaj(
    nazwa IN VARCHAR2);
    

END constructors;
/

CREATE OR REPLACE PACKAGE BODY constructors AS

  PROCEDURE addRola(
    nazwa IN VARCHAR2)
  IS
  BEGIN
    INSERT INTO T_Rola (Nazwa_roli) 
    VALUES (nazwa);
  END;

  PROCEDURE addRodzaj(
    nazwa IN VARCHAR2)
  IS
  BEGIN
    INSERT INTO T_Rodzaj (Rodzaj) 
    VALUES (nazwa);
  END;

  PROCEDURE addOsoba(
    first_name IN VARCHAR2,
    surname IN VARCHAR2,
    urodzona IN DATE,
    gender IN CHAR,
    kraj IN VARCHAR2)
  IS
  BEGIN
    INSERT INTO T_Osoba (Imie, Nazwisko, Data_urodzenia, Plec, ID_Kraj) 
    VALUES (first_name, surname, urodzona, gender, id_conversion.getKrajID(kraj));
  END;

  PROCEDURE addProgram(
    nazwa_programu IN VARCHAR2,
    nazwa_orig IN VARCHAR2,
    dozwolone IN VARCHAR2)
  IS
  BEGIN
    INSERT INTO T_Program (Tytul, Tytul_Orig, ID_OdLat) 
    VALUES (nazwa_programu, nazwa_orig, id_conversion.getDozwoloneID(dozwolone));
  END;

  PROCEDURE addTransmisja(
    nazwa_transmisji IN VARCHAR2,
    czas IN TIMESTAMP,
    dozwolone IN VARCHAR2)
  IS
  BEGIN
    INSERT INTO T_Transmisja (Nazwa, Czas_transmisji, ID_OdLat) 
    VALUES (nazwa_transmisji, czas, id_conversion.getDozwoloneID(dozwolone));
  END;

  PROCEDURE addFilm(
    nazwa IN VARCHAR2, 
    nazwa_orig IN VARCHAR2, 
    dozwolone IN VARCHAR2, 
    rok_produkcji IN NUMBER, 
    premiera IN DATE, 
    czas_trwania IN NUMBER)
  IS
  BEGIN
    INSERT INTO T_Film (Tytul, Tytul_Orig, ID_OdLat, Rok_prod, Data_premiery, Czas)  
    VALUES (nazwa, nazwa_orig, id_conversion.getDozwoloneID(dozwolone), rok_produkcji, premiera, czas_trwania); 
  END;

  PROCEDURE addSerial(
    nazwa IN VARCHAR2, 
    nazwa_orig IN VARCHAR2, 
    dozwolone IN VARCHAR2, 
    ile_sezonow IN NUMBER)
  IS
  BEGIN 
    INSERT INTO T_Serial (Tytul, Tytul_Orig, ID_OdLat, Ilosc_sezonow) 
    VALUES (nazwa, nazwa_orig, id_conversion.getDozwoloneID(dozwolone), ile_sezonow); 
  END;

  PROCEDURE addSerialSezon(
    nazwa_orig IN VARCHAR2, 
    numer_sezonu IN NUMBER,
    poczatek IN DATE) 
  IS
  BEGIN
    INSERT INTO T_Serial_Sezon (ID_Serial, Nr_sezonu, Start_emisji) 
    VALUES (id_conversion.getSerialID(nazwa_orig), numer_sezonu, poczatek);
  END;

  PROCEDURE addSerialOdcinek(
    nazwa_orig IN VARCHAR2,
    numer_sezonu IN NUMBER,
    numer_odcinka IN NUMBER,
    nazwa_odcinka IN VARCHAR2,
    czas IN NUMBER,
    emisja IN DATE)
  IS
  BEGIN
    INSERT INTO T_Serial_Odcinek (ID_Serial, Nr_sezonu, Nr_odcinka, Nazwa, Czas_trwania, Data_emisji) 
    VALUES (id_conversion.getSerialID(nazwa_orig), numer_sezonu, numer_odcinka, nazwa_odcinka, czas, emisja);
  END;

END constructors;
/
