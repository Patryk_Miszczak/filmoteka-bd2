/* wypisuje typy i nazwy obiektow (triggery, tabelki, indeksy etc.), 
 * aby pozbyc sie wszystkich obiektow i rozpoczac prace na czysto
 * spool [nazwa_pliku]
 * wykonaj ponizsze zapytanie
 * w pliku masz gotowe instrukcje usuwajace twoje obiekty.
 * czasami nalezy kilkukrotnie powtorzyc ww czynnosci aby usunac zaleznosci miedzy obiektami.
 */

select 'drop ' || object_type || ' ' || object_name || ';' from user_objects;
