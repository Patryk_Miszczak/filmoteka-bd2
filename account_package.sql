/* Pakiet zawiera procedury do obsługi kont użytkowników. */

CREATE OR REPLACE PACKAGE accounts IS
  PROCEDURE addKonto(nickname IN VARCHAR2, mail IN VARCHAR2);
  PROCEDURE verifyKonto(numer_konta IN NUMBER, first_name IN VARCHAR2, surname IN VARCHAR2, telefon IN VARCHAR2);
  PROCEDURE addKupionyMaterial(numer_konta IN NUMBER, numer_materialu IN NUMBER);
  PROCEDURE addWypozyczonyMaterial(numer_konta IN NUMBER, numer_materialu IN NUMBER, okres_czasu IN NUMBER DEFAULT 7); -- w dniach

END accounts;
/

CREATE OR REPLACE PACKAGE BODY accounts IS
  PROCEDURE addKonto(nickname IN VARCHAR2, mail IN VARCHAR2)
  IS
  BEGIN
    INSERT INTO T_Konto (Nazwa_konta, Adres_mail) 
    VALUES (nickname, mail);
  END;

  PROCEDURE verifyKonto(numer_konta IN NUMBER, first_name IN VARCHAR2, surname IN VARCHAR2, telefon IN VARCHAR2) 
  IS
  BEGIN
    INSERT INTO T_Uzytkownik (ID_Konto, Imie, Nazwisko, Nr_telefonu) 
    VALUES (numer_konta, first_name, surname, telefon); 
  END;

  PROCEDURE addKupionyMaterial(numer_konta IN NUMBER, numer_materialu IN NUMBER)
  IS
  BEGIN
    INSERT INTO T_MAP_Konto_Kupione (ID_Konto, ID_Material)
    VALUES (numer_konta, numer_materialu);
  END;

  PROCEDURE addWypozyczonyMaterial(numer_konta IN NUMBER, numer_materialu IN NUMBER, okres_czasu IN NUMBER DEFAULT 7)
  IS
    czas_uplyniecia DATE := SYSDATE + okres_czasu;
  BEGIN
    INSERT INTO T_MAP_Konto_Wypozyczone (ID_Konto, ID_Material, Poczatek, Koniec)
    VALUES (numer_konta, numer_materialu, SYSDATE, czas_uplyniecia);
  END;

END accounts;
/

