/* Tabele wiążące odpowiednie wartości numeryczne z opisowymi informacjami */

/* Ogolne etykiety okreslajace gatunek danego materialu (Akcja, Dokument, Mecz, Program rozrywkowy, itd.) */

CREATE TABLE T_Rodzaj(
	ID_Rodzaj NUMBER(3,0) NOT NULL,
	Rodzaj VARCHAR2(20) NOT NULL,
	CONSTRAINT PK_T_Rodzaj PRIMARY KEY (ID_Rodzaj) 
);

/* Ograniczenie wiekowe danego materiału. */

CREATE TABLE T_Dozwolone(
	ID_OdLat NUMBER(1,0) NOT NULL,
	Lata VARCHAR2(2) NOT NULL,
	CONSTRAINT PK_T_Dozwolone PRIMARY KEY (ID_OdLat),
	CONSTRAINT CheckLata CHECK (Lata IN ('BO','7','12','16','18'))
);

/* Numeryczne ID panstw. */

CREATE TABLE T_Kraj(
	ID_Kraj NUMBER(3,0) NOT NULL,
	Kraj VARCHAR2(60) NOT NULL,
	CONSTRAINT PK_T_Kraj PRIMARY KEY (ID_Kraj)
);

/* Numeryczne ID ról odgrywanych przez ludzi w relacjach z encjami(aktor, reżyser, ...). */

CREATE TABLE T_Rola(
  ID_Rola NUMBER(3,0) NOT NULL,
  Nazwa_roli VARCHAR2(30) NOT NULL,
  CONSTRAINT PK_T_Rola PRIMARY KEY (ID_Rola),
  CONSTRAINT T_Rola_Rola_Unique UNIQUE (Nazwa_roli)
);

/* Tabela zawiera wartości numeryczne korespondujące do właściwości 
 * jakie posiada udostępniany przez nas materiał. Chodzi tu o dostepne rozdzielczości
 * video, dostępne formaty dźwięku (DTS, Dolby Surround, etc.), napisy w danym języku,
 * dostępność materiału w 3D, ewentualne dodatkowe materiały (wywiady z aktorami, edycje reżyserskie,
 * dostępność wielu ujęć kamery, etc.). Zawiera również informacje o sposobie dostępności materiału (kupno, VOD, PPV, transmisja live, etc.).
 * Konwencja numeracji:
 *  - [1-100] --> właściwości dźwięku i wideo
 *  - [101-500] --> dostępność napisów w poszczególnych językach
 *  - [501-1500] --> materiały dodatkowe
 *  - [1501-3000] --> zarezerwowane na inne sprawy techniczne
 *  - [3001-3050] --> sposób udostępniania materiału
 */

CREATE TABLE T_Wlasciwosc(
  ID_Wlasciwosc NUMBER(4,0) NOT NULL,
  Nazwa_wlasciwosci VARCHAR2(30) NOT NULL,
  CONSTRAINT PK_T_Wlasciwosc PRIMARY KEY (ID_Wlasciwosc),
  CONSTRAINT T_Wlasciwosc_Nazwa_Unique UNIQUE (Nazwa_wlasciwosci)
);
