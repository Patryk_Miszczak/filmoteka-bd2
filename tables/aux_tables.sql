/* Tabele reprezentujące relacje wiele-do-wielu między encjami (kto grał w jakim filmie, do jakich gatunków należy) */

/* Niektore z constraintow na obce klucze lamia konwencje nazewnictwa przez ograniczenia
 * oracla w wersjach ponizej 12.2 (nazwy identyfikatorow <= 30 znakow).
 */

/* Tabela wiążąca materiał ze słowami kluczowymi które go opisują. */

CREATE TABLE T_MAP_Rodzaj_Material(
	ID_Rodzaj NUMBER(3,0) NOT NULL,
  ID_Material NUMBER(10,0) NOT NULL, -- moze pochodzic z kilku roznych tabel
  CONSTRAINT PK_T_MAP_Rodzaj_Material PRIMARY KEY (ID_Rodzaj, ID_Material),
  CONSTRAINT FK_Rodzaj_Material_Rodzaj FOREIGN KEY (ID_Rodzaj) REFERENCES T_Rodzaj (ID_Rodzaj)
  --CONSTRAINT FK_T_MAP_Rodzaj_Material_ID_Rodzaj FOREIGN KEY (ID_Rodzaj) REFERENCES T_Rodzaj (ID_Rodzaj)
);

/* Tabela wiążąca ludzi z rolami które odegrali przy tworzeniu danego materiału. */

CREATE TABLE T_MAP_Osoba_Material(
  ID_Osoba NUMBER(10,0) NOT NULL,
  ID_Rola NUMBER(3,0) NOT NULL,
  ID_Material NUMBER(10,0) NOT NULL,
  Odgrywana_rola VARCHAR2(100), -- nazwa postaci odgrywanej/dubbingowanej
  --CONSTRAINT FK_T_MAP_Osoba_Material_ID_Osoba FOREIGN KEY (ID_Osoba) REFERENCES T_Osoba (ID_Osoba),
  CONSTRAINT FK_Osoba_Material_Osoba FOREIGN KEY (ID_Osoba) REFERENCES T_Osoba (ID_Osoba),
  CONSTRAINT FK_Osoba_Material_Rola FOREIGN KEY (ID_Rola) REFERENCES T_Rola (ID_Rola)
);

/* Tabela wiążąca studia ze zrealizowanymi materiałami. */

CREATE TABLE T_MAP_Studio_Material(
	ID_Studio NUMBER(4,0) NOT NULL,
  ID_Material NUMBER(10,0) NOT NULL,
  CONSTRAINT PK_T_MAP_Studio_Material PRIMARY KEY (ID_Studio, ID_Material),
  --CONSTRAINT FK_T_MAP_Studio_Material_ID_Studio FOREIGN KEY (ID_Studio) REFERENCES T_Studio (ID_Studio)
  CONSTRAINT FK_Studio_Material_Studio FOREIGN KEY (ID_Studio) REFERENCES T_Studio (ID_Studio)
);

/* Tabela zawierająca informacje o wykupionych na własność materiałach. */

CREATE TABLE T_MAP_Konto_Kupione(
	ID_Konto NUMBER(9,0) NOT NULL,
  ID_Material NUMBER(10,0) NOT NULL,
  CONSTRAINT FK_T_MAP_Konto_Kupione FOREIGN KEY (ID_Konto) REFERENCES T_Konto (ID_Konto)
);

/* Tabela zawierająca informacje o aktywnych wypożyczonych materiałach (po upłynięciu
 * "terminu ważności" - zostaje przeniesiona w odpowiednie miejsce.
 */

CREATE TABLE T_MAP_Konto_Wypozyczone(
	ID_Konto NUMBER(9,0) NOT NULL,
  ID_Material NUMBER(10,0) NOT NULL,
  Poczatek DATE NOT NULL,
  Koniec DATE NOT NULL,
  CONSTRAINT PK_T_MAP_Konto_Wypozyczone PRIMARY KEY (ID_Konto, ID_Material),
  --CONSTRAINT FK_T_MAP_Konto_Wypozyczone_ID_Konto FOREIGN KEY (ID_Konto) REFERENCES T_Konto (ID_Konto)
  CONSTRAINT FK_Konto_Wypozyczone_Konto FOREIGN KEY (ID_Konto) REFERENCES T_Konto (ID_Konto)
);

/* Tabela przechowuje informacje o właściwościach danego materiału (patrz tabele mapujące). */

CREATE TABLE T_MAP_Material_Wlasciwosc(
  ID_Material NUMBER(10,0) NOT NULL,
  ID_Wlasciwosc NUMBER(10,0) NOT NULL,
  CONSTRAINT PK_T_MAP_Material_Wlasciwosc PRIMARY KEY (ID_Material, ID_Wlasciwosc),
  --CONSTRAINT FK_T_MAP_Material_Wlasciwosc_ID_Wlasciwosc FOREIGN KEY (ID_Wlasciwosc) REFERENCES T_Wlasciwosc (ID_Wlasciwosc)
  CONSTRAINT FK_Material_Wlasc_Wlasc FOREIGN KEY (ID_Wlasciwosc) REFERENCES T_Wlasciwosc (ID_Wlasciwosc)
);
