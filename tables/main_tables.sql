/* Deklaracje tabel zawierajacych główne i poboczne informacje o encjach. */

/* Tabele przechowujące informacje o encjach związanych z samym materiałem wideo (programy TV, filmy, seriale). */

CREATE TABLE T_Film(
	ID_Film NUMBER(10,0) NOT NULL,
	Tytul VARCHAR2(100) NOT NULL,
	Tytul_Orig VARCHAR2(100) NOT NULL,
	ID_OdLat NUMBER(1,0) NOT NULL,
	Rok_prod NUMBER(4,0) NOT NULL,
	Data_premiery DATE NOT NULL,
	Data_premiery_kraj DATE,
	Czas NUMBER(3,0) NOT NULL, -- podawany w minutach
	Budzet NUMBER(12,0),
	Zarobil NUMBER(12,0),
	CONSTRAINT PK_T_Film PRIMARY KEY (ID_Film),
  CONSTRAINT FK_T_Film_ID_OdLat FOREIGN KEY (ID_OdLat) REFERENCES T_Dozwolone ( ID_OdLat ),
	CONSTRAINT CheckRokProd CHECK ( Rok_prod > 1900 AND Rok_prod < 2100 ), -- ambitne plany :)
	CONSTRAINT CheckDataPremKraj CHECK ( Data_premiery_kraj >= Data_premiery )
);

CREATE TABLE T_Serial(
  ID_Serial NUMBER(10,0) NOT NULL,
	Tytul VARCHAR2(100) NOT NULL,
	Tytul_Orig VARCHAR2(100) NOT NULL,
	ID_OdLat NUMBER(1,0) NOT NULL, -- daty premiery itp. wypchniete do tabeli sezonow
	Ilosc_sezonow NUMBER(9,0) NOT NULL,
  Ukonczony NUMBER(1,0), -- pseudo-boolowska flaga
  CONSTRAINT PK_T_Serial PRIMARY KEY (ID_Serial),
  CONSTRAINT PK_T_Serial_ID_OdLat FOREIGN KEY (ID_OdLat) REFERENCES T_Dozwolone ( ID_OdLat )
);

/* Tabela zawiera informacje nt. innych materiałów wideo potencjalnie
 * udostępnianych w serwisie (programy przyrodnicze, kabarety, nagrania koncertów, 
 * autorskie materiały właścicieli etc.).
 */

CREATE TABLE T_Program(
  ID_Program NUMBER(10,0) NOT NULL,
	Tytul VARCHAR2(100) NOT NULL,
	Tytul_Orig VARCHAR2(100) NOT NULL,
	ID_OdLat NUMBER(1,0) NOT NULL, -- tabela zawiera malo pol aby byla mozliwie jak najbardziej wielofunkcyjna
  CONSTRAINT PK_T_Program PRIMARY KEY (ID_Program),
  CONSTRAINT PK_T_Program_ID_OdLat FOREIGN KEY (ID_OdLat) REFERENCES T_Dozwolone ( ID_OdLat )
);

/* Tabela przechowuje informacje nt. materiałów transmitowanych live (mecze, gale, koncerty etc.). */

CREATE TABLE T_Transmisja(
  ID_Transmisja NUMBER(10,0) NOT NULL,
  Nazwa VARCHAR2(100) NOT NULL,
  Czas_transmisji TIMESTAMP NOT NULL, -- data i godzina w jako strefa UTC 0:00
  ID_OdLat NUMBER(1,0) NOT NULL, -- oceny obliczane i przechowywane poza baza
  CONSTRAINT PK_T_Transmisja PRIMARY KEY (ID_Transmisja),
  CONSTRAINT PK_T_Transmisja_ID_OdLat FOREIGN KEY (ID_OdLat) REFERENCES T_Dozwolone ( ID_OdLat )
);

/* Informacje nt. poszczególnych sezonów serialu. */

CREATE TABLE T_Serial_Sezon(
  ID_Serial NUMBER(10,0) NOT NULL,
  Nr_sezonu NUMBER(3,0) NOT NULL,
  Start_emisji DATE NOT NULL,
  Koniec_emisji DATE,
	CONSTRAINT PK_T_Serial_Sezon PRIMARY KEY (ID_Serial, Nr_sezonu),
	CONSTRAINT FK_T_Serial_Sezon_ID_Serial FOREIGN KEY (ID_Serial) REFERENCES T_Serial (ID_Serial)
);

/* Informacje nt. poszczególnych odcinków. */

CREATE TABLE T_Serial_Odcinek(
  ID_Serial_Odcinek NUMBER(10,0) NOT NULL,
  ID_Serial NUMBER(10,0) NOT NULL,
  Nr_sezonu NUMBER(3,0) NOT NULL,
  Nr_odcinka NUMBER(3,0) NOT NULL,
  Nazwa VARCHAR2(100) NOT NULL,
  Czas_trwania NUMBER(3,0) DEFAULT -1 NOT NULL , -- flaga na brak danych
  Data_emisji DATE NOT NULL,
  CONSTRAINT PK_Serial_Sezon_Odcinek PRIMARY KEY (ID_Serial_Odcinek),
	CONSTRAINT FK_T_Serial_Odcinek_ID_Serial FOREIGN KEY (ID_Serial) REFERENCES T_Serial (ID_Serial)
);

/* Studia filmowe. */

CREATE TABLE T_Studio(
	ID_Studio NUMBER(4,0) NOT NULL,
	Studio VARCHAR2(50) NOT NULL,
	ID_Kraj NUMBER(3,0) NOT NULL,
	CONSTRAINT PK_T_Studio PRIMARY KEY (ID_Studio),
	CONSTRAINT FK_T_Studio_ID_Kraj FOREIGN KEY (ID_Kraj) REFERENCES T_Kraj (ID_Kraj)
);

/* Tabela przechowująca dane ludzi odgrywających różne role dla przechowywanych przez nas
 * encji - aktorzy, lektorzy, reżyserowie, muzycy, eksperci od efektów specjalnych etc.
 */

CREATE TABLE T_Osoba(
  ID_Osoba NUMBER(10,0) NOT NULL,
  Imie VARCHAR2(50) NOT NULL,
  Nazwisko VARCHAR2(50) NOT NULL,
  Data_urodzenia DATE NOT NULL,
  Plec CHAR(1) NOT NULL,
	ID_Kraj NUMBER(3,0) NOT NULL,
  CONSTRAINT PK_T_Osoba PRIMARY KEY (ID_Osoba), 
	CONSTRAINT CheckPlec CHECK (Plec IN ('M', 'F')),
	CONSTRAINT FK_T_Osoba_ID_Kraj FOREIGN KEY (ID_Kraj) REFERENCES T_Kraj (ID_Kraj)
);

/* Informacje nt. głównych jak i pobocznych kont użytkowników serwisu 
 * Każdy korzystający posiada jedno konto główne - reprezentowane przez poniższe pola.
 * Podanie danych i weryfikacja ułatwiają płatności i pozwalają na tworzenie pomocniczych kont (limit 5 podkont na 1 konto glowne):
 *  - podkont z pewnymi ograniczeniami np. dla użytku przez dzieci ze sztywną blokadą na dostępne materiały
 *  - umozliwia udostepnianie materiałów znajomym, rodzinie, narzeczonej etc.
 * jak i dają więcej możliwości przy korzystaniu z serwisu (promocje, programy lojalnościowe etc.) 
 */

/* Hasła jak i wszelkie sprawy związane z logowaniem, uwierzytelnianiem itp. są obsługiwane przez oddzielny moduł bazodanowy. */

CREATE TABLE T_Konto(
	ID_Konto NUMBER(9,0) NOT NULL,
	ID_Konto_Matka NUMBER(9,0), -- dla podkont rejestrowanych do kont głównych
  Nazwa_konta VARCHAR2(50) NOT NULL, -- koresponduje z nickiem uzywanym do logowania
  Data_urodzenia DATE,
  Adres_mail VARCHAR(80) NOT NULL, -- standard RFC dopuszcza 320 znakow, ale my ograniczamy to
  Zweryfikowane NUMBER(1,0), -- pseudo boolean (null dla pod-kont)
  Blokada_tresci CHAR(2) DEFAULT 'BO', -- blokada tresci o okreslonych wymaganiach wiekowych, domyslnie brak (wymus nulla dla kont glownych!!).
	CONSTRAINT PK_T_Konto PRIMARY KEY (ID_Konto),
	CONSTRAINT T_Konto_Nazwa_Unique UNIQUE (Nazwa_konta),
	CONSTRAINT T_Konto_Email_Unique UNIQUE (Adres_mail)
);

/* Tabela z danymi osobowymi dla zweryfikowanych kont. */

CREATE TABLE T_Uzytkownik(
  ID_Konto NUMBER(9,0) NOT NULL,
  Imie VARCHAR2(80) NOT NULL,
  Nazwisko VARCHAR2(80) NOT NULL,
  Nr_telefonu CHAR(12) NOT NULL,
  Ulica VARCHAR2(60),
  Nr_budynku VARCHAR2(10),
  Nr_mieszkania VARCHAR2(10),
  Miasto VARCHAR2(40),
  Kraj VARCHAR2(50),
  Kod_pocztowy VARCHAR2(10),
  CONSTRAINT FK_T_Uzytkownik_ID_Konto FOREIGN KEY (ID_Konto) REFERENCES T_Konto (ID_Konto)
);
